// models/Auth.js
const m = require('mithril');


const Auth = {
    username: "",
    password: "",
    isDisabled: true,

    isLoggedIn: localStorage.getItem('isLoggedIn') || false,

    setUsername: function(value) {
        Auth.username = value
        Auth.setDisabled()
    },
    setPassword: function(value) {
        Auth.password = value
        Auth.setDisabled()
    },
    setDisabled: function(){
      if(Auth.username !== "" && Auth.password !== "") Auth.isDisabled = false
      else Auth.isDisabled = true
    },
    canSubmit: function() {
        if(Auth.username === "Amer Ridani" && Auth.password === "1234"){
          console.log("login!")
          Auth.isLoggedIn = true
          localStorage.setItem('isLoggedIn', 'yep')
          Auth.login()
        }
    },
    login: function() {
      m.route.set("/welcome")
    },
}

module.exports = Auth
