// models/ActivateCustomerGroup.js
const m = require('mithril');
const ClientsData = require('../views/ClientGroups/ClientsData.js')
const Client = require('../views/ClientGroups/Client.js');



const ActivateCustomerGroup = {
  activeCustomerGroup: localStorage.getItem('activateCustomerGroup') || '',
  currentClient: {},
  listOfClients: [],



  getCurrentClient: (kundennummer)=>{
    let l = []
    let customer_group = ActivateCustomerGroup.activeCustomerGroup
    for (let client in ClientsData[customer_group]) {
      if (ClientsData[customer_group].hasOwnProperty(client)) {
        if(ClientsData[customer_group][client].kundennummer == kundennummer){
          // ActivateCustomerGroup.currentClient = ClientsData[customer_group][client]
          return ClientsData[customer_group][client]
          // break
        }
      }
    }
  },

  // reference: https://stackoverflow.com/questions/49378949/react-whats-the-best-practice-to-refresh-a-list-of-data-after-adding-a-new-el/49380335
  updateListOfClients: () => {
    // generate the lists to show the clients
    let l = []
    let customer_group = ActivateCustomerGroup.activeCustomerGroup
    for (let client in ClientsData[customer_group]) {
      if (ClientsData[customer_group].hasOwnProperty(client)) {
        l.push(
          m(Client, {
            class: '',
            name: ClientsData[customer_group][client].name,
            address_short: ClientsData[customer_group][client].address_short,
            kundennummer: ClientsData[customer_group][client].kundennummer,
          }, m('p.text-muted', "Customer Group 1")),
        )
      }
    }

    ActivateCustomerGroup.listOfClients = l
    localStorage.setItem('listOfClients', l)
  },


  setActiveGroup: function(route) {
    ActivateCustomerGroup.activeCustomerGroup = route == localStorage.getItem('activateCustomerGroup') ? '' : route
    if(route == localStorage.getItem('activateCustomerGroup')){
      localStorage.setItem('activateCustomerGroup', '')
    }else{
      localStorage.setItem('activateCustomerGroup', route)
    }
    console.log(ActivateCustomerGroup.activeCustomerGroup)

    ActivateCustomerGroup.updateListOfClients()

  }
}

module.exports = ActivateCustomerGroup
