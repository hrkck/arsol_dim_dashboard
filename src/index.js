const m = require('mithril')
// const j = require('jquery')
// const p = require('popper')
// const b = require('bootstrap')

const LoginCard = require('./views/LoginCard.js');
const ForgotPassword = require('./views/ForgotPassword.js');
const LoginPage = require('./views/LoginPage.js');

const Welcome = require('./views/Welcome.js');
const Auth = require("./models/Auth.js")

const CallCenter = require('./views/CallCenter.js');
const CustomerCenter = require('./views/CustomerCenter.js');
const CollaborationCenter = require('./views/CollaborationCenter.js');
const ReportCenter = require('./views/ReportCenter.js');
const ClaimCenter = require('./views/ClaimCenter.js');
const AdministrationCenter = require('./views/AdministrationCenter.js');
const ManageClient = require('./views/ClientGroups/ManageClient.js')


m.route(document.body, '/login', {
  // '/helloworld': {view:()=>m('', 'hello world')},
  '/login': LoginPage,
  // '/forgotpassword': ForgotPassword,
  '/welcome': {
    onmatch: () =>
      !localStorage.getItem('isLoggedIn')
        ? m.route.set('/login')
        : Welcome
  },
  '/call_center': {
    onmatch: () =>
      !localStorage.getItem('isLoggedIn')
        ? m.route.set('/login')
        : CallCenter

  },
  '/customer_center': {
    onmatch: () =>
      !localStorage.getItem('isLoggedIn')
        ? m.route.set('/login')
        : CustomerCenter

  },
  '/collaboration_center': {
    onmatch: () =>
      !localStorage.getItem('isLoggedIn')
        ? m.route.set('/login')
        : CollaborationCenter

  },
  '/report_center': {
    onmatch: () =>
      !localStorage.getItem('isLoggedIn')
        ? m.route.set('/login')
        : ReportCenter

  },
  '/claim_center': {
    onmatch: () =>
      !localStorage.getItem('isLoggedIn')
        ? m.route.set('/login')
        : ClaimCenter

  },
  '/administration_center': {
    onmatch: () =>
      !localStorage.getItem('isLoggedIn')
        ? m.route.set('/login')
        : AdministrationCenter

  },
  '/13149382483994': {
    onmatch: () =>
      !localStorage.getItem('isLoggedIn')
        ? m.route.set('/login')
        : ManageClient

  }
})
// m.render(document.body, '/', 'hello world!')
