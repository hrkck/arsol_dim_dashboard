const m = require('mithril')
const SmallCentersRow = require('./SmallCentersRow.js')
const Header = require('./Header.js')


const ClaimCenter = {
  view: () => {
    return m('div.container-fluid.pl-0', {
        style: style
      },

      m(Header),

      m('div.container.pt-5', {
          style: hubStyle
        },
        m(SmallCentersRow, {
          selected: 'claim_center'
        }),
        // continue here
      )
    )
  }
}
const style = {
  // 'background-image': "url('./media/welcome_background.png')",
  // 'background-repeat': "no-repeat",
  // 'background-size': 'contain',
  'background-color': "#eaeaea",
  'min-height': '100vh'

}
const hubStyle = {
  'background-color': 'white',
  'margin': '0 auto',
  // 'height': '60%',
  // 'height': '100vh',
  'border-radius': '1%',
}


module.exports = ClaimCenter;
