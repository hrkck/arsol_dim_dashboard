// views/Login.js
const m = require('mithril');
const Auth = require("../models/Auth")
const ResetPassword = require("../models/ResetPassword")
const ForgotPassword = require("./ForgotPassword")


const LoginCard = {
  view: function() {
    return m('div.align-self-center', {style: style},
      m('h1.text-center.text-white.font-weight-light', 'Arsol Dashboard'),
      m('p.mb-5.text-center.text-white.font-weight-lighter', 'Application locked. Please login.'),

      m("form", {style: formStyle},
        m(".form-group",
          // m("label[for='exampleInputEmail1']", "Email address"),
          m("input.form-control[id='exampleInputUsername'][placeholder='Username'][type='text']", {oninput: (e)=>{Auth.setUsername(e.target.value)}, value: Auth.username}),
          // m("small.form-text.text-muted[id='emailHelp']", "We'll never share your email with anyone else.")
        ),
        m(".form-group",
          // m("label[for='exampleInputPassword1']", "Password"),
          m("input.form-control[id='exampleInputPassword1'][placeholder='Password'][type='password']", {oninput: (e)=>{Auth.setPassword(e.target.value)}, value: Auth.password})
        ),
        m(".form-group.form-check",
        m("a.stretched-link[href='#'].float-right.mb-3.font-weight-lighter", {onclick: ResetPassword.setIsResetPassword },"Forgot password?")
          // m("input.form-check-input[id='exampleCheck1'][type='checkbox']"),
          // m("label.form-check-label[for='exampleCheck1']", "Check me out")
        ),
        m("button.btn.btn-primary[type='signin'].w-100", {disabled: Auth.isDisabled, onmousedown: Auth.canSubmit}, "Sign In")
      )
    )
  }
}

const style = {
  'width': "320px",
  'height': "480px",
  'margin': '0 auto',
}

const formStyle = {
  'padding': "30px",

  'border-radius': "5%",
  'box-shadow': "0px 0px 10px 1px #fff",
  'background': "white"
}

module.exports = LoginCard;
