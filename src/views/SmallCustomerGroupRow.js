const m = require('mithril');
const SmallCustomerGroup = require('./SmallCustomerGroup.js');


const SmallCustomerGroupRow = {
  view: (vnode) => {
    return m('div',
      m('div.row.justify-content-center',
        m(SmallCustomerGroup, {
          class: 'col-2',
          route: 'customer_group_1',
          selected: vnode.attrs.selected,
        }, m('p.text-muted', "Customer Group 1")),
        m(SmallCustomerGroup, {
          class: 'col-2',
          selected: vnode.attrs.selected,
          route: 'customer_group_2',
        }, m('p.text-muted', "Customer Group 2")),
        m(SmallCustomerGroup, {
          class: 'col-2',
          selected: vnode.attrs.selected,
          route: 'customer_group_3',
        }, m('p.text-muted', "Customer Group 3")),
        m(SmallCustomerGroup, {
          class: 'col-2',
          selected: vnode.attrs.selected,
          route: 'customer_group_4',
        }, m('p.text-muted', "Customer Group 4")),
        m(SmallCustomerGroup, {
          class: 'col-2',
          selected: vnode.attrs.selected,
          route: 'customer_group_5',
        }, m('p.text-muted', "Customer Group 5")),
        m(SmallCustomerGroup, {
          class: 'col-2',
          selected: vnode.attrs.selected,
          route: 'customer_group_6',
        }, m('p.text-muted', "Customer Group 6")),
      )
    )
  }
}


const ruleStyle = {
  'height': '4px',
  // 'color': 'blue',
  // 'border': 'none',
  'border-radius': '5px',
  'background-color': 'blue'
}

module.exports = SmallCustomerGroupRow;
