const m = require('mithril');
const SmallCenter = require('./SmallCenter.js');


const SmallCentersRow = {
  view: (vnode) => {
    return m('div',
      m('div.row.justify-content-center',
        m(SmallCenter, {
          class: 'col-2',
          route: 'call_center',
          selected: vnode.attrs.selected,
          src: "./media/call_center.png"
        }, "Call Center"),
        m(SmallCenter, {
          class: 'col-2',
          selected: vnode.attrs.selected,
          route: 'customer_center',
          src: "./media/customer_center.png"
        }, "Customer Center"),
        m(SmallCenter, {
          class: 'col-2',
          selected: vnode.attrs.selected,
          route: 'collaboration_center',
          src: "./media/collaboration_center.png"
        }, "Collaboration Center"),
        m(SmallCenter, {
          class: 'col-2',
          selected: vnode.attrs.selected,
          route: 'report_center',
          src: "./media/report_center.png"
        }, "Report Center"),
        m(SmallCenter, {
          class: 'col-2',
          selected: vnode.attrs.selected,
          route: 'claim_center',
          src: "./media/claim_center.png"
        }, "Claim Center"),
        m(SmallCenter, {
          class: 'col-2',
          selected: vnode.attrs.selected,
          route: 'administration_center',
          src: "./media/administration_center.png"
        }, "Administration Center"),
      ),
      m('div.row.justify-content-center',
        m('hr.col-8', {
          style: ruleStyle
        })
      )
    )
  }
}


const ruleStyle = {
  'height': '4px',
  // 'color': 'blue',
  // 'border': 'none',
  'border-radius': '5px',
  'background-color': 'blue'
}

module.exports = SmallCentersRow;
