// ./views/ManageClient.js

const m = require('mithril')
const Header = require('../Header.js')

const ActivateCustomerGroup = require('../../models/ActivateCustomerGroup.js')


const ManageClient = {
  route: '',
  currentClient: {},
  oninit: (vnode) => {
    let kundennummer = m.route.get().replace('/', '')
    vnode.state.route = kundennummer
    ActivateCustomerGroup.updateListOfClients()
    vnode.state.currentClient = ActivateCustomerGroup.getCurrentClient(kundennummer)
  },
  view: (vnode) =>
    m('div.container-fluid.pl-0', {
        style: style
      },

      m(Header),

      m('div.container.pl-5', {
          style: hubStyle
        },

        m('.row.justify-content-end', {style: buttonRowStyle},
          m(".input-group-append.m-4",
            m("button.btn.btn-outline-secondary[id='SearchTagsCancelButton'][type='button']", {
              onclick: () => {
                m.route.set('/customer_center')
              }
            }, m.trust("&times;")))
        ),


        m('br'),
        m('div.row.justify-content-around.text-left.align-items-center',
          m('br'),
          m("img.col-4.img-fluid.card-img-top.d-block", {
            style: imgStyle,
            src: './media/amer_ridani.jpg'
          }),
          m('h1.col-8', vnode.state.currentClient.name)
        ),

        m('br'), m('br'), m('br'),

        m('div.row.justify-content-around.text-left.align-items-center',
          m('.container',
            m('.row.justify-content-around.text-left.align-items-center',
              m('div.col-4',
                m('p.font-weight-bold', 'Kundennummer'),
                m('p', vnode.state.currentClient.kundennummer),
              ),
              m('div.col-4',
                m('p.font-weight-bold', 'Addresse'),
                m('p', vnode.state.currentClient.address),
              ),
            )
          )
        ),
        m('br'), m('br'), m('br'),
        m('div.row.justify-content-around.text-left.align-items-center',
          m('.container',
            m('.row.justify-content-around.text-left.align-items-center',
              m('div.col-4',
                m('p.font-weight-bold', 'Placeholder1'),
                m('p', 'Content1'),
              ),
              m('div.col-4',
                m('p.font-weight-bold', 'Placeholder2'),
                m('p', 'Content 2'),
              ),
            )
          )
        ),
        m('br'), m('br'), m('br'),
        m('br'), m('br'), m('br'),
      )
    )
}


const style = {
  // 'background-image': "url('./media/welcome_background.png')",
  // 'background-repeat': "no-repeat",
  // 'background-size': 'contain',
  'background-color': "#eaeaea",
  'min-height': '100vh'

}
const hubStyle = {
  'background-color': 'white',
  'margin': '0 auto',

  // 'height': '60%',
  // 'height': '100vh',
  'border-radius': '1%',
}
const imgStyle = {
  // 'position': 'relative',
  // 'float': 'right',
  'max-width': '18%',
  'height': 'auto',
  'border-radius': '100%',
  // 'background-position': '50% 50%',
  // 'background-repeat': 'no-repeat',
  // 'background-size': 'cover',
}

const buttonRowStyle = {
  'float': 'right',
  "margin-top": "2px",
  "margin-bottom": "2px"
}


module.exports = ManageClient;
