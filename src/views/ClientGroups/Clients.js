// ./views/Clients.js

const m = require('mithril')

const ActivateCustomerGroup = require('../../models/ActivateCustomerGroup.js')


const Clients = {
  oninit: (vnode) => {
    if (ActivateCustomerGroup.activeCustomerGroup) {
      ActivateCustomerGroup.updateListOfClients()
    }
  },
  view: (vnode) =>
    m('div.container',

      m('div.row.justify-content-around',
        m('h3.col-2', 'Clients'),
        m('div.col-auto'),
        m(".input-group.col-3",
          m("input.form-control[aria-describedby='SearchTags'][aria-label='SearchTags'][placeholder='Search Client'][type='text']"),
        ),
        // m('div.col-1'),
      ),
      m('br'),
      m('br'),

      m('div.container',
        m('div.row.justify-content-between',
          ActivateCustomerGroup.listOfClients.map(item => item)
        )
      )
    )
}


module.exports = Clients;
