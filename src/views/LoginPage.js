const m = require('mithril');
const LoginCard = require('./LoginCard');
const ForgotPassword = require('./ForgotPassword');
const ResetPassword = require("../models/ResetPassword")


const LoginPage = {
  view: () => {
    return m('div.container-fluid.row', {
        style: style
      },
      ResetPassword.isResetPassword ?
        m(ForgotPassword) :
        m(LoginCard)
    )
  }
}


const style = {
  'background-image': "url('./media/background.svg')",
  'background-repeat': "no-repeat",
  'background-size': 'cover',
  'height': '100vh',
  'width': '100vw',
}


module.exports = LoginPage;
