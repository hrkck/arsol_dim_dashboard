const m = require('mithril');
const ActivateCustomerGroup = require('../models/ActivateCustomerGroup.js');


const SmallCustomerGroup = {
  view: (vnode) => {
    return m('div.m-4.text-center', {
        onclick: () => {
          // console.log(vnode.attrs.route)
          // m.route.set('/' + vnode.attrs.route)
        }
      },
      m(".card.text-center", {
          style: style,
          style: {
            'border': vnode.attrs.selected == vnode.attrs.route ? '2px solid #314cc4' : ''
          },
          onclick: () => {
            ActivateCustomerGroup.setActiveGroup(vnode.attrs.route)
          }
        },
        m("img.p-2.card-img-top.rounded.mx-auto.d-block", {
          style: imgStyle,
          src: './media/customer_group.png'
        })),
        m(".card-body.p-0.mt-2.text-center", {
            style: textStyle
          },
          m("p.card-text.p-0", vnode.attrs.selected == vnode.attrs.route ? vnode.children : ''))


    )

  }
}

// const ActiveSelection = {
//   mouseEnter: (e) => {
//     e.target.style.border = '3px solid #314cc4'
//     e.target.style['padding-top'] = '28px' // the picture wont saccade on mouseEnter
//   },
//   mouseLeave: (e) => {
//     e.target.style.border = ''
//     e.target.style['padding-top'] = '30px'
//   },
// }


const style = {
  'border-radius': "3%",
  'background': "white",
  'height': "50px",
  'width': "50px",
  // 'padding-top': "30px",
  'border': '',
}
const textStyle = {
  'font-size': '9px',
  'color': '#314cc4'
}
const imgStyle = {
  'position': 'relative',
  // 'float': 'right',
  'width': '45px',
  'height': '45px',
  'background-position': '50% 50%',
  'background-repeat': 'no-repeat',
  'background-size': 'cover',
}

module.exports = SmallCustomerGroup;
