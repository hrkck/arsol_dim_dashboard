const m = require('mithril');
const ActivateCustomerGroup = require('../models/ActivateCustomerGroup.js');


const CustomerGroup = {
  view: (vnode) => {
    return m('div.pt-4.text-center', {
        onclick: () => {
          // console.log(vnode.attrs.route)
          // m.route.set('/' + vnode.attrs.route)
        }
      },
      m(".card.text-center", {
          style: style,
          onmouseenter: (e) => {
            ActiveSelection.mouseEnter(e)
          },
          onmouseleave: (e) => {
            ActiveSelection.mouseLeave(e)
          },
          onclick: () => {
            ActivateCustomerGroup.setActiveGroup(vnode.attrs.route)
          }
        },
        m("img.card-img-top.rounded.mx-auto.d-block", {
          style: imgStyle,
          src: './media/customer_group.png'
        }),
        m(".card-body.p-0",
          m("p.card-text.p-0", vnode.children))
      )
    )
  }
}

const ActiveSelection = {
  mouseEnter: (e) => {
    e.target.style.border = '3px solid #314cc4'
    e.target.style['padding-top'] = '28px' // the picture wont saccade on mouseEnter
  },
  mouseLeave: (e) => {
    e.target.style.border = ''
    e.target.style['padding-top'] = '30px'
  },
}


const style = {
  'border-radius': "5%",
  'box-shadow': "0px 0px 20px 1px lightgrey",
  'background': "white",
  'height': "180px",
  'width': "180px",
  'padding-top': "30px",
  'border': '',
}

const imgStyle = {
  'position': 'relative',
  // 'float': 'right',
  'width': '100px',
  'height': '90px',
  'background-position': '50% 50%',
  'background-repeat': 'no-repeat',
  'background-size': 'cover',
}

module.exports = CustomerGroup;
