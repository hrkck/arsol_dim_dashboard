const m = require('mithril')

const Header = {
  view: () => {
    return m('div.container-fluid.text-center.mb-5', {
          style: headerStyle
        },
        m('.row',
          m('.col-12.mt-3',
            m('h2.text-white', 'Arsol Dashboard'),
            m('h3.text-white.font-weight-light', 'Home'),
          )
        )
      )
  }
}

const headerStyle = {
  'background': '#314cc4',
  // 'height': '15vh'
}


module.exports = Header;
