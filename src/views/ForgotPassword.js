// views/ForgotPassword.js
const m = require('mithril');
const ResetPassword = require("../models/ResetPassword")


const ForgotPassword = () => {
  let email = ""

  let isDisabled = true
  let isClicked = false


  let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

  const updateEmail = (e) => {
    email = e.target.value
    isDisabled = false
  }
  const validateEmail = () => { if (email.match(mailformat))isClicked = true }

  return {
    view: function() {
      return m('div.align-self-center', {
          style: style
        },
        m('h1.text-center.text-white.font-weight-light', 'Arsol Dashboard'),
        m('p.mb-5.text-center.text-white.font-weight-lighter', 'Enter your e-mail to reset your password'),

        m("form", {
            style: formStyle
          },
          m(".form-group",
            m("input.form-control[aria-describedby='email'][id='exampleEmail'][placeholder='Email'][type='email']", {
              oninput: (e) => {
                updateEmail(e)
              },
              value: email
            }),
            isClicked ?
            m("small.form-text.text-muted[id='report']", "You should receive an e-mail shortly.") :
            m('', '')
          ),

          m('div.container',
            m('div.row.justify-content-end.m-0',
              m('div.col-12.align-self-center.p-0',
                m("button.btn.btn-primary[type='button'].w-100", {
                  disabled: isDisabled,
                  onclick: validateEmail
                }, "Reset Password")
              ),
            )
          ),
          m('div.container.mt-3',
            m('div.row.justify-content-end.m-0',
              m('div.col-6.align-self-end.pr-0',
                m("button.btn.btn-primary[type='goback'].w-100", {
                  onclick: () => {
                    ResetPassword.isResetPassword = false
                  }
                }, "go back")
              )
            )
          )
        )
      )
    }
  }
}

const style = {
  // 'position':'relative',
  'width': "320px",
  'height': "480px",
  'margin': '0 auto',
  // 'background': "white",

}

const formStyle = {
  // 'position':'relative',
  // 'height': "auto",
  // 'margin': 'auto ',
  'padding': "30px",

  'border-radius': "5%",
  'box-shadow': "0px 0px 10px 1px #fff",
  'background': "white",
  // 'padding': "20px",
  // 'border-radius': "5%",
  // 'box-shadow': "0px 0px 20px 1px #fff"
}

module.exports = ForgotPassword;
