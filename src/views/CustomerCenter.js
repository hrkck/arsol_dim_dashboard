const m = require('mithril')
const SmallCentersRow = require('./SmallCentersRow.js')
const Header = require('./Header.js')
const CustomerGroup = require('./CustomerGroup.js');
const CustomerGroupRow = require('./CustomerGroupRow.js');
const SmallCustomerGroupRow = require('./SmallCustomerGroupRow.js');

const Clients = require('./ClientGroups/Clients.js');

const ActivateCustomerGroup = require('../models/ActivateCustomerGroup.js')

const CustomerCenter = {
  view: () => {
    return m('div.container-fluid.pl-0', {
        style: style
      },

      m(Header),

      m('div.container.pt-5', {
          style: hubStyle
        },
        m(SmallCentersRow, {
          selected: 'customer_center'
        }, ),
        // continue here:


        m('br'),

        m('div.container', {
            class: ActivateCustomerGroup.activeCustomerGroup === '' ? 'd-none' : '',
          },
          m(SmallCustomerGroupRow, {
            selected: ActivateCustomerGroup.activeCustomerGroup
          }),

          m('div.text-center',
            m(Clients),
          ),

          m('br'),

        ),

        m('div.container', {
            class: ActivateCustomerGroup.activeCustomerGroup === '' ? '' : 'd-none'
          },
          m('div.row.justify-content-around',
            m('h3.col-2', 'Filter'),
            m('div.col-auto'),
            m(".input-group.col-3",
              m("input.form-control[aria-describedby='SearchTags'][aria-label='SearchTags'][placeholder='Search Group'][type='text']"),
            ),
            // m('div.col-1'),
          ),
          m('div',
            m(CustomerGroupRow),
          ),

          m('br'),

        )
      )
    )
  }
}
const style = {
  // 'background-image': "url('./media/welcome_background.png')",
  // 'background-repeat': "no-repeat",
  // 'background-size': 'contain',
  'background-color': "#eaeaea",
  'min-height': '100vh'

}
const hubStyle = {
  'background-color': 'white',
  'margin': '0 auto',
  // 'height': '60%',
  // 'height': '100vh',
  'border-radius': '1%',
}



module.exports = CustomerCenter;
