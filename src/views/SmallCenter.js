const m = require('mithril');

const SmallCenter = {
  view: (vnode) => {
    return m('div',
      m('div.m-2', {
          onclick: () => {
            if(m.route.get() == '/'+vnode.attrs.route){
              m.route.set('/welcome')
            }else{
              m.route.set('/' + vnode.attrs.route)
            }
          }
        },
        m(".card.text-center.p-4", {
            style: style,
            style: {
              'border': vnode.attrs.selected == vnode.attrs.route ? '2px solid #314cc4' : ''
            }
          },
          m("img.card-img-top.rounded.mx-auto.d-block", {
            style: imgStyle,
            src: vnode.attrs.src
          }),
        ),
        m(".card-body.p-0.mt-2.text-center", {
            style: textStyle
          },
          m("p.card-text.p-0", vnode.attrs.selected == vnode.attrs.route ? vnode.children : ''))
      )
    )
  }
}


const style = {
  'border-radius': "3%",
  'box-shadow': "0px 0px 20px 1px gray",
  'background': "white",
  'height': "50px",
  'width': "50px",
  // 'padding-top': "30px",
  'border': '',
}
const textStyle = {
  'font-size': '9px',
  'color': '#314cc4'
}
const imgStyle = {
  'position': 'relative',
  // 'float': 'right',
  'width': '45px',
  'height': '40px',
  'background-position': '50% 50%',
  'background-repeat': 'no-repeat',
  'background-size': 'cover',
}

module.exports = SmallCenter;
