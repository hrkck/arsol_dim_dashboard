const m = require('mithril');
const Center = require('./Center.js');
const Header = require('./Header.js');


const Welcome = {
  view: () => {
    return m('div.container-fluid.pl-0', {
        style: style
      },

      m(Header),


      m('div.container',
        m('div.container.pt-5.pb-5', {
            style: hubStyle
          },
          m('div.row.pl-4.ml-5',
            m('.col-12',
              m('h3.font-weight-normal', 'Hi Amer Ridani'),
            ),
            m('.col-12',
              m('p.font-weight-light', 'Welcome back!'),
            ),
            m('.col-12',
              m('h4.font-weight-bold', 'Select a center'),
            ),
          ),

          m('div.row.justify-content-around.pb-3',
            m(Center, {
              route: 'call_center',
              src: "./media/call_center.png"
            }, "Call Center"),
            m(Center, {
              route: 'customer_center',
              src: "./media/customer_center.png"
            }, "Customer Center"),
            m(Center, {
              route: 'collaboration_center',
              src: "./media/collaboration_center.png"
            }, "Collaboration Center"),
          ),
          m('div.row.justify-content-around.pt-3',
            m(Center, {
              route: 'report_center',
              src: "./media/report_center.png"
            }, "Report Center"),
            m(Center, {
              route: 'claim_center',
              src: "./media/claim_center.png"
            }, "Claim Center"),
            m(Center, {
              route: 'administration_center',
              src: "./media/administration_center.png"
            }, "Administration Center")
          )
        )
      )
    )
  }
}

const style = {
  // 'background-image': "url('./media/welcome_background.svg')",
  // 'background-repeat': "no-repeat",
  // 'background-size': 'cover',
  'background-color': "#eaeaea",
  'height': "100vh",
}


const hubStyle = {
  'background-color': 'white',
  'margin': '0 auto',
  'height': '60%',
  'border-radius': '1%',
}



module.exports = Welcome;
